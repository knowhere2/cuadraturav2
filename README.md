# Proyecto Cuadratura

Este es el proyecto **Cuadratura**. A continuación, se detallan los pasos necesarios para configurar el entorno de desarrollo e instalar las dependencias necesarias.

## Requisitos previos

Antes de comenzar, asegúrate de tener instalados los siguientes programas:

- **[Miniconda](https://docs.conda.io/en/latest/miniconda.html)** o **[Anaconda](https://www.anaconda.com/products/individual)** para gestionar entornos virtuales y dependencias.
- **Git** para clonar el repositorio.

## Pasos para la instalación

### 1. Clonar el repositorio

Primero, clona este repositorio en tu máquina local utilizando Git:

```bash
git clone https://gitlab.com/tu_usuario/tu_repositorio.git
cd tu_repositorio
```
### 2. Creae un entorno virtual

```bash
conda env create -f environment.yml

```

### 3. Activar entorno virtual

```bash
conda activate dev
```

### 5. Desactivar entorno virtual

```bash
conda deactivate

```

### 6. Actualizar las dependencias

```bash
conda env update -f environment.yml

```



### Explicación del contenido del `README.md`:

1. **Clonar el repositorio**: El primer paso es clonar el proyecto desde GitLab a tu máquina local.
2. **Crear el entorno virtual**: Se utiliza el archivo `environment.yml` para crear un entorno virtual con las dependencias definidas.
3. **Activar el entorno**: Se activa el entorno Conda para que puedas trabajar en el proyecto.
4. **Instalar dependencias adicionales**: Si se necesitan paquetes adicionales, se menciona cómo instalar los paquetes usando `pip` dentro del entorno Conda.
5. **Verificar instalación**: Se incluyen instrucciones para verificar si las dependencias están correctamente instaladas.
6. **Ejecutar pruebas**: Si el proyecto tiene pruebas automatizadas, se puede ejecutar `pytest` para comprobar que todo funciona correctamente.
7. **Desactivar el entorno**: Después de trabajar, se desactiva el entorno con `conda deactivate`.
8. **Contribuir**: Se proporcionan instrucciones básicas sobre cómo contribuir al proyecto a través de GitLab.


