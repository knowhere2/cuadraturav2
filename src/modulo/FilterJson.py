class FilterJson:
    excluded_statuses = {"Canceled", "Suspended"}  # Estados que queremos excluir
    flujos = {0,4,33}

    def __init__(self, listJson):
        self.listJson = listJson

    def filterJsonInfo(self, data):
        transaction = self.listJson.get("PosLog", {}).get("Transaction", {})
        retail_transaction = transaction.get("RetailTransaction", {})

        status = retail_transaction.get("TransactionStatus")
        store = transaction.get("RetailStoreID")
        actionCode = transaction.get("ActionCode")

        if (status is None) and (store == data) and (actionCode in self.flujos):
            return self.listJson
