class Reception:

    RECAUDOS = {
        216: 'Recarga telefónica',
        217: 'Anulación de Donación',
        324: 'Recaudo Colpatria',
        415: 'Recaudos genéricos-Shopping center',
        482: 'Recaudos genéricos-Exequias',
        52: 'Recaudo donación',
        57: 'Recaudo Claro',
        54: 'Avance',
        55: 'Anulacion Avance',
        56: 'Recaudo tarjetas Incommm',
        58: 'Recaudo Moto',
        61: 'Recaudo Food',
        62: 'Recaudo Callcenter',
        63: 'Recaudo Convenios fidelidad',
        65: 'Recaudo Donación Unicef',
        78: 'Recaudo avance tarjeta compensar',
        79: 'Recaudo avance billetera compensar',
        96: 'Consulta bono secretaria',
        98: 'Recaudo Activación de Giftcard Física',
        99: 'Caja Menor'
    }

    def __init__(self):
        pass

    def get_reception_type(self, subcode):
        receipt_list = []
        receipt_information = {}

        for data in self.RECAUDOS.keys():
            if subcode == data:
                receipt_information["RECAUDO"] = self.RECAUDOS[data]
                receipt_list.append(receipt_information)
                return receipt_list
