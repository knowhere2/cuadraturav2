class Rounding:

    def __init__(self,data):
        self.data = data

    def get_rounding_donation(self):
        payment_list = []
        medios = {}
        line_item = self.data["PosLog"]["Transaction"]["RetailTransaction"]["LineItem"]
        ajuste = False
        for item_interno in line_item:

            if "Tender" in item_interno:
                if "Rounding" in item_interno["Tender"]:
                    medios["REDONDEO"] = float(item_interno["Tender"]["Rounding"])
                    medios["DONACION"] = None
                    payment_list.append(medios)
                    ajuste = True
                else:
                    if "Donation" in item_interno["Tender"]:
                        medios["REDONDEO"] = None
                        medios["DONACION"] = float(item_interno["Tender"]["Donation"])
                        payment_list.append(medios)
                        ajuste = True
                    else:
                        continue

        if ajuste:
            None
        else:
            medios["REDONDEO"] = None
            medios["DONACION"] = None
            payment_list.append(medios)

        return payment_list
