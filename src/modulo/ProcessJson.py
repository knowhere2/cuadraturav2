import json


class ProcessJson:
    trx = ''
    excluded_statuses = {"Canceled", "Suspended"}

    def __init__(self, trx):
        self.trx = trx

    def read_json(self):
        with open(self.trx, "r", encoding='utf8') as file:
            data = json.load(file)
        return data
