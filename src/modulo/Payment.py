class Payment:

    def __init__(self, data):
        self.data = data


    def get_payment_method(self):
        payment_list = []
        medios = {}
        transaction_info = self.data["PosLog"]["Transaction"]
        line_item = transaction_info["RetailTransaction"]["LineItem"]

        def add_amount(medios_dict, key, amount):
            if key in medios_dict:
                medios_dict[key] += amount
            else:
                medios_dict[key] = amount

        for item_interno in line_item:
            refund = 0
            sales = 0
            if "Tender" in item_interno:
                tender = item_interno["Tender"]
                if "TenderID" in tender:
                    tender_id = tender["TenderID"]
                    type = tender["TypeCode"]
                    amount = int(float(tender["Amount"]))

                    if "TenderChange" in tender:
                        amount -= int(float(tender["TenderChange"]["Amount"]))

                    if type == "Refund":
                        refund = amount * (-1)
                    else:
                        sales = amount

                    match tender_id:
                        case 1:
                            if type == "Refund":
                                add_amount(medios, "EFECTIVO", refund)
                            add_amount(medios, "EFECTIVO", sales)
                        case 5:
                            add_amount(medios, "CONTRAENTREGAECOMM", amount)
                        case 6:
                            add_amount(medios, "MERCADOPAGO", amount)
                        case 7:
                            add_amount(medios, "ECOMMCONTINGENCIA", amount)
                        case 8:
                            add_amount(medios, "CALLCENTER", amount)
                        case 9:
                            add_amount(medios, "VALES", amount)
                        case 10:
                            if tender["CreditDebit"]["TypeCode"] == "PAYU":
                                add_amount(medios, "PAYU", amount)
                            else:
                                add_amount(medios, "TARJCREDEB", amount)
                        case 11:
                            add_amount(medios, "TARJCREDEBOFF", amount)
                        case 12:
                            add_amount(medios, "PUNTOS", amount)
                        case 13:
                            add_amount(medios, "DAVIPLATA", amount)
                        case 14:
                            add_amount(medios, "CORNERSHOP", amount)
                        case 15:
                            add_amount(medios, "CUPONES", amount)
                        case 16:
                            add_amount(medios, "VANTI-BRILLA", amount)
                        case 17:
                            add_amount(medios, "BILLETERA", amount)
                        case 19:
                            add_amount(medios, "GIFTCARDVIRTUAL", amount)
                        case 20:
                            add_amount(medios, "SODEXO", amount)
                        case 21:
                            if "Donation" in tender:
                                amount -= int(float(tender["Donation"]))
                            add_amount(medios, "BIGPASS", amount)
                        case 22:
                            add_amount(medios, "BONOTRANSFBANCARI", amount)
                        case 24:
                            add_amount(medios, "RAPPY", amount)
                        case 27:
                            add_amount(medios, "COMPENSARTOKEN", amount)
                        case 28:
                            add_amount(medios, "CREDITO ESPECIAL", amount)
                        case 29:
                            add_amount(medios, "GLOBALPAY", amount)
                        case 30:
                            add_amount(medios, "RETEICA", amount)
                        case 31:
                            add_amount(medios, "BONOSECRETARIADIS", amount)
                        case 32:
                            add_amount(medios, "TARJETACOMPENSAR", amount)
                        case 33:
                            add_amount(medios, "TPAGA", amount)
                        case 34:
                            add_amount(medios, "TARJETAREGALO", amount)
                        case 35:
                            add_amount(medios, "REDENCIONLIFEMILES", amount)
                        case 36:
                            add_amount(medios, "TARJETAREGALOACTV", amount)
                        case 37:
                            add_amount(medios, "NOTADEVOLUCIONOFF", amount)
                        case 38:
                            if "Avance_TC" in medios:
                                amount = -amount
                            add_amount(medios, "AVANCETC", amount)
                        case 39:
                            add_amount(medios, "CAJAMENORTC", amount)

        payment_list.append(medios)
        return payment_list
