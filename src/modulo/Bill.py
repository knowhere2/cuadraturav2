class Bill:

    def __init__(self,data):
        self.data = data

    def get_invoice_information(self):
        invoice_list = []
        factura = {}
        try:
            customer = self.data["PosLog"]["Transaction"]["RetailTransaction"]["Customer"]
            if customer["CustomerID"] == "222222222222":
                factura["FACTURA"] = 3
            else:
                factura["FACTURA"] = 2

        except KeyError:
            factura["FACTURA"] = 1

        invoice_list.append(factura)
        return invoice_list
