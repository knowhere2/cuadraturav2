import pandas as pd

from modulo.Bill import Bill
from modulo.Header import Header
from modulo.Payment import Payment
from modulo.Product import Product
from modulo.Reception import Reception
from modulo.Rounding import Rounding
from modulo.Total import Total


class ConcatenateSales:

    def __init__(self,data):
        self.data = data


    def generate_return_sales(self):

        def frame_glue(*args):
            return pd.concat([i for i in args], axis=1)

        header_frame_return = pd.DataFrame(Header(self.data).get_list_headers())
        item_information_frame_return = pd.DataFrame(Product(self.data).get_item_information())
        total_values_information_frame_return = pd.DataFrame(Total(self.data).get_total_values())
        redoung_method_frame_return = pd.DataFrame(Rounding(self.data).get_rounding_donation())
        invoice_information_frame_return = pd.DataFrame(Bill(self.data).get_invoice_information())
        final_data_frame_return_sales = frame_glue(header_frame_return,item_information_frame_return,total_values_information_frame_return, redoung_method_frame_return,invoice_information_frame_return)
        final_data_frame_return_sales["FACTURA"] = final_data_frame_return_sales['FACTURA'].fillna(method='ffill')
        final_data_frame_return_sales["TOTAL"] = final_data_frame_return_sales['BASE'] + final_data_frame_return_sales['IVA_VALOR'] + final_data_frame_return_sales['IPO_VALOR']
        return final_data_frame_return_sales

    def generate_return_reception(self, subcode):
        def frame_glue(*args):
            return pd.concat([i for i in args], axis=1)

        header_frame_return = pd.DataFrame(Header(self.data).get_list_headers())
        reception_type = pd.DataFrame(Reception().get_reception_type(subcode))
        total_values_information_frame_return = pd.DataFrame(Total(self.data).get_total_values())
        redoung_method_frame_return = pd.DataFrame(Rounding(self.data).get_rounding_donation())
        final_data_frame_return_reception = frame_glue(header_frame_return,reception_type,total_values_information_frame_return,redoung_method_frame_return)
        return final_data_frame_return_reception

    def generate_return_payment(self):
        def frame_glue(*args):
            return pd.concat([i for i in args], axis=1)

        header_frame_return = pd.DataFrame(Header(self.data).get_list_headers())
        payment_frame_return = pd.DataFrame(Payment(self.data).get_payment_method())
        final_data_frame_return_payment = frame_glue(header_frame_return,payment_frame_return)
        return final_data_frame_return_payment