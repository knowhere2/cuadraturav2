class Header:

    def __init__(self, data):
        self.data = data

    def get_list_headers(self):
        list_header = []
        header = {}
        header["CAJERO"] = self.data["PosLog"]["Transaction"]["Operator"]["EmployeeID"]
        header["TIENDA"] = self.data["PosLog"]["Transaction"]["RetailStoreID"]
        header["POS"] = self.data["PosLog"]["Transaction"]["WorkstationID"]
        header["TRANSACCION"] = self.data["PosLog"]["Transaction"]["SequenceNumber"]
        list_header.append(header)
        return list_header
