import pandas as pd


class CreateExcel:
    xlsx_file = '../resources/output.xlsx'


    def write_to_excel(self, result_return_sales, result_return_refund, result_return_reception, result_return_payment):
        with pd.ExcelWriter(self.xlsx_file) as writer:
            if result_return_sales.empty:
                pass
            else:
                result_return_sales.to_excel(writer, sheet_name="Ventas", index=False)

            if result_return_refund.empty:
                pass
            else:
                result_return_refund.to_excel(writer, sheet_name="Devoluciones", index=False)

            if result_return_reception.empty:
                pass
            else:
                result_return_reception.to_excel(writer, sheet_name="Recaudos", index=False)

            if result_return_payment.empty:
                pass
            else:
                result_return_payment.to_excel(writer, sheet_name="MDP", index=False)

            print("Se ha generado el archivo exitosamente C:")