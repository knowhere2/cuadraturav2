class Product:
    def __init__(self, data):
        self.data = data

    def get_item_information(self):
        items_list = []
        line_item = self.data["PosLog"]["Transaction"]["RetailTransaction"]["LineItem"]
        for item_interno in line_item:
            bases = {}
            if "Tax" in item_interno and "POSIdentity" in item_interno:
                for intro in item_interno.get("Tax"):
                    ean_Unico = item_interno.get('POSIdentity', {}).get('POSItemID')
                    if ean_Unico:
                        bases["EAN"] = ean_Unico

                    if intro["TaxType"] == 'IVA':
                        bases["BASE"] = float(intro["BaseAmount"])
                        bases["IVA"] = intro["TaxGroupID"]
                        bases["IVA_VALOR"] = float(intro["Amount"])

                    if intro["TaxType"] == 'IMPO':
                        bases["IPO"] = intro["TaxGroupID"]
                        bases["IPO_VALOR"] = float(intro["Amount"])
                    else:
                        bases["IPO"] = 0
                        bases["IPO_VALOR"] = 0
                items_list.append(bases)
        return items_list
