class Total:
    def __init__(self, data):
        self.data = data

    def get_total_values(self):
        total_values_list = []
        line_total = self.data["PosLog"]["Transaction"]["RetailTransaction"]["Total"]
        valor_total = {}
        ajuste = False
        for total in line_total:
            if "TransactionDiscountAmount" in total.values():
                valor_total["DESCUENTO"] = float(total["Amount"])
                ajuste = True
            elif not ajuste:
                valor_total["DESCUENTO"] = None

            if "TransactionBaseAmount" in total.values():
                valor_total["SUBTOTAL"] = float(total["Amount"])
                total_values_list.append(valor_total)

        return total_values_list
