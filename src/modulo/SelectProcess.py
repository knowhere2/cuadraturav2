import pandas as pd

from modulo.ConcatenateSales import ConcatenateSales


class SelectProcess:
    sales = {0, 4}
    refund = {1, 2, 3}
    reception = {33}

    result_return_sales = pd.DataFrame()
    return_sales = []
    result_return_refund = pd.DataFrame()
    return_refund = []
    result_return_reception = pd.DataFrame()
    return_reception = []
    result_return_payment = pd.DataFrame()
    return_payment = []

    def __init__(self):
        pass

    def selectProcesInfo(self, data):

        if data is not None:
            actionCode = data["PosLog"]["Transaction"].get("ActionCode")
            subCode = data["PosLog"]["Transaction"].get("SubCode")

            # Proceso para devoluciones
            if (subCode in self.refund) and (actionCode in self.sales):
                self.return_refund.append(ConcatenateSales(data).generate_return_sales())
                self.result_return_refund = pd.concat(self.return_refund)

            # Proceso para ventas
            if (subCode is None) and (actionCode in self.sales):
                self.return_sales.append(ConcatenateSales(data).generate_return_sales())
                self.result_return_sales = pd.concat(self.return_sales)

            # Proceso para recaudos
            if actionCode in self.reception:
                self.return_reception.append(ConcatenateSales(data).generate_return_reception(subCode))
                self.result_return_reception = pd.concat(self.return_reception)

        self.return_payment.append(ConcatenateSales(data).generate_return_payment())
        self.result_return_payment = pd.concat(self.return_payment)


