from kafka import KafkaConsumer
import json

from modulo.CreateExcel import CreateExcel
from modulo.FilterJson import FilterJson
from modulo.ProcessJson import ProcessJson
from modulo.SelectProcess import SelectProcess

# Yo me encargo de ejecutar el script c:
if __name__ == '__main__':

    archivo_json = 'resources/poslog.json'
    data = ProcessJson(archivo_json).read_json()
    final = SelectProcess()

    store_input = input("Ingrese el numero de la tienda: ")
    process = input("Obtener informacion del poslog del kafka(s/n): ").lower()

    if store_input.isdigit() and len(store_input) == 4:
        store = int(store_input)
        print(f"Store set to: {store}")
        print(f"Valor de process: {process}")
        if process.isalpha() and len(process) == 1 and process == 's':
            try:
                kafka_brokers = 'brk0.kafka-test-co.cencosud.net:443'
                cert_location = 'KafkaConsumer/kafka-certificate.pem'
                key_location = 'KafkaConsumer/kafka-key.pem'
                ca_file = 'KafkaConsumer/kafka-truststore.pem'
                topic = 'pos.sm.co.ventas'
                password = 'Kolonvia00.'
                security_protocol = 'SSL'
                sasl_mechanism = "PLAIN"

                consumer = KafkaConsumer(topic,
                                         bootstrap_servers=kafka_brokers,
                                         auto_offset_reset='latest',
                                         enable_auto_commit=False,
                                         security_protocol=security_protocol,
                                         ssl_check_hostname=True,
                                         ssl_cafile=ca_file,
                                         # ssl_mechanism=sasl_mechanism,
                                         # ssl_plain_username="MyUser",
                                         # ssl_plain_password=password,
                                         ssl_certfile=cert_location,
                                         ssl_keyfile=key_location,
                                         value_deserializer=lambda x: json.loads(x.decode('utf-8')),
                                         consumer_timeout_ms=6000000
                                         )
                # 6000000ms 50 min

                save_file = open("resources/PoslogProcesado.json", "w")
                for item in consumer:
                    print(item.value)
                    dataForProces = FilterJson(item.value).filterJsonInfo(store)
                    if dataForProces is not None:
                        final.selectProcesInfo(dataForProces)
                    CreateExcel().write_to_excel(final.result_return_sales, final.result_return_refund,
                                                  final.result_return_reception, final.result_return_payment)
                    json.dump(item.value, save_file, indent=3)

                save_file.close()
            except Exception as e:
                print("Error con las variables de configuracion y/o datos de entrada")
                print(e)

        elif process.isalpha() and len(process) == 1 and process == 'n':
            try:
                for item in data:
                    print(item)
                    dataForProces = FilterJson(item).filterJsonInfo(store)
                    if dataForProces is not None:
                        final.selectProcesInfo(dataForProces)

                CreateExcel().write_to_excel(final.result_return_sales, final.result_return_refund,final.result_return_reception, final.result_return_payment)

            except Exception as e:
                print("Error con las variables de configuracion y/o datos de entrada")
                print(e)





